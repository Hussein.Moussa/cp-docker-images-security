#!/usr/bin/env bash

keytool -import -keystore ../jre/lib/security/cacerts -trustcacerts -alias "VeriSign Class 3 International Server CA - G3" -file /pathto/SVRIntlG3.cer

keytool -keystore /etc/kafka/secrets/kafka.broker.truststore.jks -alias CARoot -import -file /etc/kafka/secrets/snakeoil-ca-1.crt -storepass confluent -noprompt