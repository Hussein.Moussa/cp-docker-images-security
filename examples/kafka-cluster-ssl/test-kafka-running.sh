#!/usr/bin/env bash

docker run \
    --network kafka-cluster-ssl_default \
    --rm \
    confluentinc/cp-kafka \
    kafka-topics --create --topic bar --partitions 3 --replication-factor 3 --if-not-exists \
    --zookeeper zookeeper-1:22181


docker run \
    --network kafka-cluster-ssl_default \
    --rm \
    confluentinc/cp-kafka \
    kafka-topics --describe --topic bar \
    --zookeeper zookeeper-1:22181



docker run \
  --network kafka-cluster-ssl_default \
  --rm \
  -v ${KAFKA_SSL_SECRETS_DIR}:/etc/kafka/secrets \
  confluentinc/cp-kafka \
  bash -c "seq 42 | kafka-console-producer --broker-list broker1:19092,broker2:29092,broker3:39092 --topic bar -producer.config /etc/kafka/secrets/host.producer.ssl.config && echo 'Produced 42 messages.'"