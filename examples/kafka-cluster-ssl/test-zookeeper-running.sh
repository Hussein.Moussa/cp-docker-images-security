#!/usr/bin/env bash

for i in 1 2 3; do
   port=$(( ($i+1)*10000+2181 ))
   server="zookeeper-$i"
   echo "$server:$port"
   docker run --rm --network kafka-cluster-ssl_default confluentinc/cp-zookeeper bash -c "echo stat | nc $server $port | grep Mode"
done